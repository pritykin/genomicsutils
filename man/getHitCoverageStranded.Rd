% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/footprinting.R
\name{getHitCoverageStranded}
\alias{getHitCoverageStranded}
\title{Calculate coverage around hits taking into account strand of hits and reads}
\usage{
getHitCoverageStranded(gr, coverage.plus, coverage.minus, radius = NULL,
  mc.cores = 2)
}
\arguments{
\item{gr}{GRanges object, e.g., with TF motif hits obtained using FIMO.}

\item{coverage.plus, coverage.minus}{RleList objects with coverage of plus
minus stranded reads (single 5'-most base pair from each read counted)}

\item{radius}{Integer for radius of region around hit for which to calculate
coverage. If NULL, determine automatically as 3 times width of hits.}

\item{mc.cores}{Integer. How many cores to use in parallel.}
}
\value{
List with two elements:
        "sense": Matrix of integer counts. Rows correspond to ranges in gr,
                 and the number of columns is motif width + 2 * radius.
                 Matrix contains counts of "sense" reads with respect to
                 motif, i.e., reads aligning in the same direction as motif.
        "antisense": same as "sense", but for "antisense" reads, i.e.,
                     aligned opposite to motif direction.
}
\description{
Calculate coverage around hits taking into account strand of hits and reads
}
